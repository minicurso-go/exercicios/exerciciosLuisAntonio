package main

import "fmt"

func calcularPotencia(base, expoente int) int {
	resultado := 1
	for i := 0; i < expoente; i++ {
		resultado *= base
	}
	return resultado
}

func main() {
	var base, expoente int
	fmt.Print("Digite a base")
	fmt.Scan(&base)
	fmt.Print("Digite o expoente")
	fmt.Scan(&expoente)
	resultado := calcularPotencia(base, expoente)

	fmt.Printf("%d elevado a %d é igual a %d\n", base, expoente, resultado)
}

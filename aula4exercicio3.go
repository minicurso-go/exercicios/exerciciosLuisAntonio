package main

import "fmt"

func media(n1 int, n2 int) int {
	return (n1 + n2) / 2
}

func main() {
	for {
		var n1, n2 int

		fmt.Print("Digite um numero ")
		fmt.Scan(&n1)
		fmt.Print("Digite um numero ")
		fmt.Scan(&n2)

		m := media(n1, n2)
		fmt.Println("media dos ultimos 2 numeros= ", m)

	}
}

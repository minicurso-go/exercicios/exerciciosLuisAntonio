package main

import "fmt"

func fatorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * fatorial(n-1)
}

func main() {
	var numero int
	fmt.Print("Digite um número para calcular o fatorial: ")
	fmt.Scanln(&numero)

	resultado := fatorial(numero)
	fmt.Print("O fatorial é ", resultado)
}

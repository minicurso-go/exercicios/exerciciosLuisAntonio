package main

import "fmt"

func soma(n1 int, n2 int) int {
	return n1 + n2
}
func main() {
	var n1, n2 int
	fmt.Print("Primeiro numero:")
	fmt.Scan(&n1)
	fmt.Print("Segundo numero:")
	fmt.Scan(&n2)

	s := soma(n1, n2)

	fmt.Print("Soma =", s)
}

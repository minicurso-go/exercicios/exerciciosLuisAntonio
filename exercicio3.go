package main

import "fmt"

func main() {
	var a, b, c float64
	fmt.Print("Digite primeiro valor")
	fmt.Scan(&a)
	fmt.Print("Digite segundo valor")
	fmt.Scan(&b)
	fmt.Print("Digite terceiro valor")
	fmt.Scan(&c)

	fmt.Println("A- A area do triangulo AB é ", (a*b)/2)
	fmt.Println("B- A area do circulo de raio c é ", (c*c)*3.14159)
	fmt.Println("C- A area do trapezio ABC é ", ((a+b)*c)/2)
	fmt.Println("D- A area do quadrado B é ", b*b)
	fmt.Println("E- A area do retangulo AB é ", a*b)
}

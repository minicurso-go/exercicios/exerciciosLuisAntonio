package main

import (
	"fmt"
)

func verificarPrimo(num int) bool {

	if num < 2 {
		return false
	}

	for i := 2; i*i <= num; i++ {
		if num%i == 0 {
			return false
		}
	}
	return true
}

func main() {

	var numero int
	fmt.Print("Digite o numero")
	fmt.Scan(&numero)

	if verificarPrimo(numero) {
		fmt.Printf("%d é um número primo.\n", numero)
	} else {
		fmt.Printf("%d não é um número primo.\n", numero)
	}
}

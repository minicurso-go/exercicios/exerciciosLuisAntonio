package main

import "fmt"

func main() {
	var n1 float64
	var n2 float64
	var codigo int

	fmt.Print("Digite o primeiro numero")
	fmt.Scan(&n1)
	fmt.Print("Digite o segundo numero")
	fmt.Scan(&n2)
	fmt.Print("Qual codigo da operacao?")
	fmt.Scan(&codigo)

	switch codigo {
	case 1:
		fmt.Print("Soma= ", n1+n2)
	case 2:
		fmt.Print("Subtracao=", n1-n2)
	case 3:
		fmt.Print("Divisao=", n1/n2)
	case 4:
		fmt.Print("Multiplicao=", n1*n2)
	}
}

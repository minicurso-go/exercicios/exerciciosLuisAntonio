package main

import "fmt"

func main() {
	var numero float64

	fmt.Print("Digite seu numero")
	fmt.Scan(&numero)

	for i := 1.0; i <= numero; i++ {
		divi := numero / i
		fmt.Println(divi)
	}

}

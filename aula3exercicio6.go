package main

import "fmt"

func main() {
	var n1 int
	var maior int
	for {
		fmt.Println("Digite um numero")
		fmt.Scan(&n1)

		if n1 > maior {
			maior = n1
		}

		if n1 == 0 {
			fmt.Println("Seu maior numero foi", maior)
			break
		}

	}
}
